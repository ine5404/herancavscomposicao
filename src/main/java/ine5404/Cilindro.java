/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ine5404;

/**
 *
 * @author martin.vigil
 */
public class Cilindro {
    protected double altura;
    protected Circulo circulo;
    
    public Cilindro(double altura, double raio){
        this.circulo = new Circulo(raio);
        this.altura = altura;
              
    }
    
    public double getArea(){
        return 2 * this.circulo.getArea() + 2 * Math.PI * this.circulo.getRaio();
    }
    
    public double getVolume(){
        return this.circulo.getArea() * this.altura;
    }
}
