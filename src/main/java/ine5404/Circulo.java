/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ine5404;

/**
 *
 * @author martin.vigil
 */
public class Circulo {
    protected double raio;
    
    public Circulo(double raio){
        this.raio = raio;
    }
    
    public double getRaio(){
        return this.raio;
    }
    
    public double getArea(){
        return Math.PI * this.raio * this.raio;
    }
}
